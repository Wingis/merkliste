import 'dart:convert';

Note noteFromJson(String str) {
  final jsonData = json.decode(str);
  return Note.fromMap(jsonData);
}

String noteToJson(Note data) {
  final dyn = data.toMap();
  return json.encode(dyn);
}

class Note {
  int id;
  String title;
  String description;
  bool isDone;

  Note({
    this.id,
    this.title,
    this.description,
    this.isDone,
  });

  factory Note.fromMap(Map<String, dynamic> json) => new Note(
        id: json["id"],
        title: json["title"],
        description: json["description"],
        isDone: json["isDone"] == 1,
      );

  Map<String, dynamic> toMap() => {
        "id": id,
        "title": title,
        "description": description,
        "isDone": isDone,
      };
}
