import 'package:flutter/material.dart';
import 'package:merkliste/BLOCS/DatabaseBloc.dart';
import 'package:merkliste/NoteModel.dart';

import 'dart:math' as math;

void main() => runApp(MaterialApp(home: MyApp()));

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  // data for testing
  List<Note> testNotes = [
    Note(title: "Raouf", description: "Rahiche", isDone: false),
    Note(title: "Zaki", description: "oun", isDone: true),
    Note(title: "oussama", description: "ali", isDone: false),
  ];

  final bloc = NotesBloc();

  @override
  void dispose() {
    bloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Notizliste")),
      body: StreamBuilder<List<Note>>(
        stream: bloc.notes,
        builder: (BuildContext context, AsyncSnapshot<List<Note>> snapshot) {
          if (snapshot.hasData) {
            return ListView.builder(
              itemCount: snapshot.data.length,
              itemBuilder: (BuildContext context, int index) {
                Note item = snapshot.data[index];
                return Dismissible(
                  key: UniqueKey(),
                  background: Container(color: Colors.red),
                  onDismissed: (direction) {
                    bloc.delete(item.id);
                  },
                  child: ListTile(
                    title: Text(item.description),
                    leading: Text(item.id.toString()),
                    trailing: Checkbox(
                      onChanged: (bool value) {
                        bloc.blockUnblock(item);
                      },
                      value: item.isDone,
                    ),
                  ),
                );
              },
            );
          } else {
            return Center(child: CircularProgressIndicator());
          }
        },
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () async {
          Note rnd = testNotes[math.Random().nextInt(testNotes.length)];
          bloc.add(rnd);
        },
      ),
    );
  }
}
