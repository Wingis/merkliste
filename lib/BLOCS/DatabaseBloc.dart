import 'dart:async';

import 'package:merkliste/NoteModel.dart';
import 'package:merkliste/Database.dart';

class NotesBloc {
  final _noteController = StreamController<List<Note>>.broadcast();

  get notes => _noteController.stream;

  dispose() {
    _noteController.close();
  }

  getNotes() async {
    _noteController.sink.add(await DBProvider.db.getAllNotes());
  }

  NotesBloc() {
    getNotes();
  }

  blockUnblock(Note note) {
    DBProvider.db.toggleIsDone(note);
    getNotes();
  }

  delete(int id) {
    DBProvider.db.deleteNote(id);
    getNotes();
  }

  add(Note note) {
    DBProvider.db.newNote(note);
    getNotes();
  }
}
